describe('API Routes Testing', function() {

    describe('GET /api/indexing', function(done){
	    it('return status 200 when scraping data kurs from BCA website', function(done){
	    	this.timeout(10000);

		    request.get('/api/indexing')
                .expect(200)
                .end(function(err, res) {
                    done(err);
                });
	    })
	})

    describe('POST /api/kurs', function() {
        it('insert a new kurs', function(done) {
        	var res = {
				    status: 200,
				    data: [
				        {
				            symbol: 'ANR',
				            e_rate: {
				                jual: 1803.55,
				                beli: 20
				            },
				            tt_counter: {
				                jual: 1803.55,
				                beli: 200
				            },
				            bank_notes: {
				                jual: 1803.55,
				                beli: 2000
				            },
				            date: '2099-03-20'
				        }
				    ]
				}
            request.post('/api/kurs')
                .send({
					symbol: 'ANR',
					e_rate: {
						jual: 1803.55,
						beli: 20
					},
					tt_counter: {
						jual: 1803.55,
						beli: 200
					},
					bank_notes: {
						jual: 1803.55,
						beli: 2000
					},
					date: '2099-03-20'
				})
                .expect(200)
                .end(function(err, res) {
                	expect(res).to.eql(res);
                    done(err);
                });
        });
    });

    describe('PUT /api/kurs', function() {
        it('update a kurs', function(done) {
        	var res = {
				    status: 200,
				    data: [
				        {
				            symbol: 'ANR',
				            e_rate: {
				                jual: 20,
				                beli: 20
				            },
				            tt_counter: {
				                jual: 200,
				                beli: 200
				            },
				            bank_notes: {
				                jual: 2000,
				                beli: 2000
				            },
				            date: '2099-03-20'
				        }
				    ]
				}
            request.put('/api/kurs')
                .send({
					symbol: 'ANR',
					e_rate: {
						jual: 20,
						beli: 20
					},
					tt_counter: {
						jual: 200,
						beli: 200
					},
					bank_notes: {
						jual: 2000,
						beli: 2000
					},
					date: '2099-03-20'
				})
                .expect(200)
                .end(function(err, res) {
                	expect(res).to.eql(res);
                    done(err);
                });
        });
    });

    describe('GET /api/kurs?startdate={date-1}&enddate={date-2}', function() {
        it('return all kurs according to the parameters given', function(done) {
        	var res = {
				    status: 200,
				    data: [
				        {
				            symbol: 'ANR',
				            e_rate: {
				                jual: 20,
				                beli: 20
				            },
				            tt_counter: {
				                jual: 200,
				                beli: 200
				            },
				            bank_notes: {
				                jual: 2000,
				                beli: 2000
				            },
				            date: '2099-03-20'
				        }
				    ]
				}
            request.get('/api/kurs?startdate=2099-03-20&enddate=2099-03-20')
                .expect(200)
                .end(function(err, res) {
                	expect(res).to.eql(res);
                    done(err);
                });
        });
    });

    describe('GET /api/kurs/:symbol?startdate=:startdate&enddate=:enddate', function() {
        it('return all kurs according to the parameters and symbol given', function(done) {
        	var res = {
				    status: 200,
				    data: [
				        {
				            symbol: 'ANR',
				            e_rate: {
				                jual: 20,
				                beli: 20
				            },
				            tt_counter: {
				                jual: 200,
				                beli: 200
				            },
				            bank_notes: {
				                jual: 2000,
				                beli: 2000
				            },
				            date: '2099-03-20'
				        }
				    ]
				}
            request.get('/api/kurs/ANR?startdate=2099-03-20&enddate=2099-03-20')
                .expect(200)
                .end(function(err, res) {
                	expect(res).to.eql(res);
                    done(err);
                });
        });
    });

    describe('DELETE /api/kurs/:date', function() {
        it('delete a kurs', function(done) {
        	var date = '2099-03-20'
        	var res = {
				    status: 200,
				    data: "Data berhasil di hapus"
				}
            request.delete('/api/kurs/' + date)
                .expect(200)
                .end(function(err, res) {
                	expect(res).to.eql(res);
                    done(err);
                });
        });
    });
});