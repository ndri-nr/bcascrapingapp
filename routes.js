'use strict';

module.exports = function(app) {
    var kursApi = require('./controller');

    app.route('/api/indexing')
        .get(kursApi.index);

    app.route('/api/kurs')
        .get(kursApi.getKurs);

    app.route('/api/kurs')
        .put(kursApi.updateKurs);

    app.route('/api/kurs')
        .post(kursApi.insertKurs);

    app.route('/api/kurs/:symbol')
        .get(kursApi.getKursByCurrency);

    app.route('/api/kurs/:date')
        .delete(kursApi.deleteKurs);
};