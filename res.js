'use strict';

exports.ok = function(values, res) {
	var data = {
	  	'status': 200,
	  	'data': values
	};
	res.json(data);
	res.end();
};

exports.error = function(error, res) {
	var data = {
	  	'status': 404,
	  	'message': error
	};
	res.json(data);
	res.end();
};